package com.example.zephyrdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZephyrDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZephyrDemoApplication.class, args);
    }

}
